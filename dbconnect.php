<?php

$dbhost = 'localhost';
$dbname = 'login-and-register';
$dbusername = 'root';
$dbpassword = '';

$errorMessage = 'There is no database connection available at the moment';

try {
    $dbconnect = new PDO("mysql:host=".$dbhost."; dbname=".$dbname, $dbusername, $dbpassword);
    $dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbconnect->SetAttribute(PDO::ATTR_EMULATE_PREPARES, false);

} catch(PDOException $error) {
    die("<p class='font-333 font-Arimo'>$errorMessage <br><br>" . $error->getMessage() . "</p>");
}