<?php require_once("dbconnect.php");

if(isset($_POST['register'])) {

    $post = $_POST;

    $username = $post['username'];
    $password = $post['password'];
    $email = $post['email'];

    $hashed_password = password_hash($password, PASSWORD_DEFAULT);

    if($_POST['username']) {
        if($_POST['email']) {
            if($_POST['password']) {
                $user_query = $dbconnect->prepare("INSERT INTO users (username, email, password) VALUES (:username, :email, :password)");
                $user_query->execute([
                    ':username' => $username,
                    ':password' => $hashed_password,
                    ':email' => $email,
                ]);

                $users = $user_query->fetchAll();
            }
        } else {
            echo "You need to fill in an email";
        }
    } else {
        echo "You need to fil in an username";
    }
}

?>
<form action="register.php" method="POST">
    <label for="username">Username</label>
    <input type="text" id="username" name="username" placeholder="Username" required>
    <label for="email">Email address</label>
    <input type="text" id="email" name="email" placeholder="Email address" required>
    <label for="password">Password</label>
    <input type="password" id="password" name="password" placeholder="Password" required>
    <button type="submit" id="register" name="register">Submit</button>
    <a href="login.php">Login</a>
</form>
