<?php require('dbconnect.php'); ?>
<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>
<body>
<?php
if (isset($_POST['login'])) {

    $username = $_POST['username'];
    $password = $_POST['password'];

    $hashed_password = password_hash($password, PASSWORD_DEFAULT);
    $username = filter_var($username, FILTER_SANITIZE_STRING);
    $password = filter_var($password, FILTER_SANITIZE_STRING);
    
    if (isset($_POST['username'])) {
        if (isset($_POST['password'])) {

            $query = $dbconnect->prepare("SELECT COUNT(`id`) FROM `users` WHERE `username`=:username AND `password`=:password");
            $query->execute(array('username' => $username, 'password' => $password));

            $count = $query->fetchColumn();
            if (password_verify($password, $hashed_password)) {
                if ($count == "1") {

                    if ((isset($_POST['login']))) {
                        echo '<script>window.location="dashboard";</script>';
                        end();
                    }

                } else {
                    echo "Wrong username or password combination";
                }
            }

        } else {
            echo "Password required";
        }

    } else {
        echo "Username required";
    }
}
?>
<form action="" method="POST">
    <label for="username">Username</label>
    <input type="text" id="username" name="username" placeholder="Username" required>
    <label for="password">Password</label>
    <input type="password" id="password" name="password" placeholder="Password" required>
    <button type="submit" id="login" name="login">Login</button>

    <a href="register.php">Register</a>
</form>
</body>
</html>
